# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

"""
The WIDE IO social network
"""

from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType


import wioframework.fields as models
from wioframework.amodels import *
from django.contrib import messages
import extfields.fields as ef


from wioframework import decorators as dec
from wioframework import gen2
from django.db.models import Q
from django.core.mail import send_mail
from django.template.loader import render_to_string
import settings

import accounts.models as wm
ANNOUNCEMENT_MAX_LENGTH = 2000
MESSAGE_MAX_LENGTH = 32000
SUBJECT_MAX_LENGTH = 386

from django.conf import settings
User = wm.UserAccount

MODELS = []

#TODO: Split in separate in model files


@wideio_owned("requester",personal=0)
@wideio_timestamped
@wideiomodel
class NDA(models.Model):
    """
    A Non Disclosure Agreement

    The right to access a specific object and related confidential information.
    The requester must request a right to access the information
    """
    ## ALL LINKS
    object_pk = models.TextField('object ID', null=True,db_index=True)
    content_object = gen2.GenericForeignKey2("object_pk")
    requester_signed=models.BooleanField(default=False)
    requester_signature=models.TextField(blank=True,db_index=True) #< EXPLICIT CONSENT
    issuer_signed=models.BooleanField(default=False)
    issuer_signature=models.TextField(blank=True,db_index=True) #< EXPLICIT CONSENT
    _nda_salt=models.TextField(blank=True,db_index=True) #< SPECIFIC SIGNATURE USED TO IDENTIFY NDA LEAKS IN LINKS , ETC...    
    

    ## TODO :: ADD DEFAULT QUERY
    @staticmethod
    def can_add(request):
        return False
    
    def can_update(self,request):
        return False
    
    def can_delete(self,request):
        return False    
    
    def can_view(self,request):
        return request.user.is_staff or (self.requester==request.user) or (content_object.owner==request.user)
    
    class WIDEIO_Meta:
        class Actions:
            @wideio_action(lambda s,r:r.user==s.requester)
            def requester_sign(self,request):
                self.requester_signed=True
                self.requester_signature=str(datetime.datetime.now())
                ## TODO: SEND EMAIL

            @wideio_action(lambda s,r:r.user==s.content_object.owner)            
            def issuer_sign(self,sign):
                self.issuer_signed=True
                self.issuer_signature=str(datetime.datetime.now())
                ## TODO: SEND EMAIL
                
            
            
MODELS.append(NDA)
MODELS += get_dependent_models(NDA)    



@wideio_commentable()
@wideio_owned("author",personal=0)
@wideio_timestamped
@wideiomodel
class Announcement(models.Model):

    """
    An announcement is a declaration made by a user
    """
    body = models.TextField(
        verbose_name="Comment",
        max_length=ANNOUNCEMENT_MAX_LENGTH,
        app_null=False,
        db_null=False,
        app_blank=False,
        db_blank=False)
    content_type = models.ForeignKey(
        ContentType,
        null=True,
        verbose_name='content type',
        related_name="content_type_set_for_%(class)s")
    object_pk = models.TextField('object ID', null=True,db_index=True)
    content_object = gen2.GenericForeignKey2("object_pk")
    comments_opened = models.BooleanField(default=True)

    def __unicode__(self):
        return self.body

    def can_comment(self, r):
        return self.comments_opened

    @staticmethod
    def create_internal(author, body, content=None, comments_opened=True,to=None):
        x = Announcement()
        x.author = author
        x.body = body
        x.content_object = content
        x.wiostate = 'V'
        x.comments_opened = comments_opened
        x.save()
        if to!=None:
          for f in to:
            n = Notification()
            n.notified_user = f
            n.content_object = x
            n.save()            
        return x

    @staticmethod
    def list_queryset_filter(queryset, request):
        # Q(to=request.user) | Q(cc__in=request.user) removed -> no field "to"
        return queryset.filter(Q(author=request.user))

    def on_comment(self, request, comment):
        username = (comment.author or request.user).username
        recipient_list = [self.author]

        # Is the user guaranteed to be the one who generated the announcement?

        for follower in request.user.get_followers():
                 if follower not in recipient_list:            
                    recipient_list.append(follower)
        for follower in self.author.get_followers():
                 if follower not in recipient_list:
                        recipient_list.append(follower)

        utils.wio_send_email(
                request,
                consent="new_comment",
                subject='There is a new comment by ' + str(comment.author) +" on "+ str(self),
                template='static/email_comment.html',
                context={
                    'x': self ,
                    'author': comment.author,
                    'comment' : comment
                    },
                to_users=recipient_list,
                url_notification=None)            


    def on_add(self, request):
        # SEND NOTIFICATION TO THE ANNOUNCEMENT AUTHOR
        self.save()
        n = self.Notification()
        n.notified_user = request.user
        n.content_object = self
        n.save()
        # SEND NOTIFICATION TO THE ANNOUNCEMENT AUTHOR'S FOLLOWERS
        for follower in request.user.get_followers():
            n = Notification()
            n.notified_user = follower
            n.content_object = self
            n.save()

    class WIDEIO_Meta:
        permissions = dec.perm_write_for_logged_users_only
        sort_enabled = ["created_at", "author"]
        search_enabled = "body"
        form_exclude = [
            'author',
            'object_pk',
            'content_object',
            'content_type']

MODELS.append(Announcement)
MODELS += get_dependent_models(Announcement)

from django.contrib import messages


@wideio_owned("author",personal=0) #we want other user to be able to open messages
@wideio_timestamped
@wideiomodel
@wideio_setnames("direct message")
class DirectMessage(models.Model):

    """
    A message from another to another (or to a group of user)
    """
    to = models.ForeignKey(
        wm.UserAccount,
        blank=False,
        null=False,
        related_name="recipient_of")
    cc = models.ManyToManyField(
        wm.UserAccount,
        related_name="in_cc_of",
        blank=True)
    subject = models.TextField(
        verbose_name="Subject",
        max_length=SUBJECT_MAX_LENGTH,
        app_null=False,
        db_null=False,
        app_blank=False,
        db_blank=False)
    body = models.TextField(
        verbose_name="Message",
        max_length=MESSAGE_MAX_LENGTH,
        app_null=False,
        db_null=False,
        app_blank=False,
        db_blank=False)
    _threadid = models.CharField(
        max_length=38,
        default=lambda: str(uuid.uuid1()))
    _in_reply_to = models.ForeignKey('network.DirectMessage', null=True)
    #content_type = models.ForeignKey(
    #    ContentType,
    #    null=True,
    #    verbose_name='content type',
    #    related_name="content_type_set_for_%(class)s")
    object_pk = models.TextField('object ID', null=True)
    content_object = gen2.GenericForeignKey2(
        #ct_field="content_type",
        fk_field="object_pk")

    def __unicode__(self):
        return self.body

    def expand_teams(self, cc):
        return [cc]

    def on_add(self, request):
        self.save()
        # SEND NOTIFICATION TO THE ANNOUNCEMENT AUTHOR'S FOLLOWERS
       
        n = Notification()
        for x in self.expand_teams(self.to):
          n.notified_user = x
          n.content_object = Announcement.create_internal(
            self.author,
            "You have received a direct message",
            self,
            comments_opened=False)
          n.save()
        
        for tcc in self.cc.all():
          for cc in self.expand_teams(tcc):
            n = Notification()
            n.notified_user = cc
            n.content_object = Announcement.create_internal(
                self.author,
                "You are in copy this direct message",
                self,
                comments_opened=False)
            n.save()
            
        messages.add_message(
            request,
            messages.INFO,
            'Your message has been sent.'
        )

    def can_update(self, request):
        return (self.wiostate != 'V')

    class WIDEIO_Meta:
        icon = "icon-envelope"
        permissions = dec.perm_write_for_logged_users_only
        sort_enabled = ["created_at", "author"]
        search_enabled = "body"
        form_exclude = [
            'author',
            'object_pk',
            'content_object',
            'content_type',
            '_threadid',
            '_in_reply_to']
        allow_query = [
            'to']

        class Actions:
            @wideio_action(icon="icon-reply",
                           possible=lambda o,
                           r: (r.user == o.to or r.user in o.cc.all()),
                           mimetype="text/html")
            def reply(self, request):
                nm = DirectMessage()
                nm._threadid = self._threadid
                nm._in_reply_to = self._in_reply_to or self
                nm.author = request.user
                if nm._in_reply_to is self:
                    # then this is the first reply
                    nm.subject = "Re: " + self.subject
                else:
                    nm.subject = self.subject
                nm.to = self.author
                nm.cc = self.cc.all()
                nm.body = "\n".join(
                    map(lambda l: "> " + l, self.body.split("\n")))
                nm.wiostate = 'U1'
                nm.save()
                return HttpResponseRedirect(nm.get_update_url())

            @wideio_action(icon="icon-forward",
                           possible=lambda o,
                           r: (r.user == o.author,
                               r.user == o.to or r.user in o.cc.all()),
                           mimetype="text/html")
            def forward(self, request):
                nm = DirectMessage()
                nm._threadid = self._threadid
                nm._in_reply_to = self._in_reply_to
                nm.author = request.user
                nm.subject = "Fw: " + self.subject
                nm.to = self.author
                nm.body = "\n".join(
                    map(lambda l: "> " + l, self.body.split("\n")))
                nm.wiostate = 'U1'
                nm.save()
                return HttpResponseRedirect(nm.get_update_url())

    # def can_add(self,request):
        # return request.user!=None
    # def  on_add(self,request):
           # for f in request.user.followers:
                # n=Notification()
                # n.notified_user=f.useraccount
                # n.content_object=self
                # n.save()

    # def  can_update(self,request):
        # return False
MODELS.append(DirectMessage)
MODELS += get_dependent_models(DirectMessage)


@wideio_owned("notified_user", personal=1)
@wideio_timestamped
@wideiomodel
class Notification(models.Model):

    """
    The notification that is displayed on the wall
    """
    content_object = models.ForeignKey(Announcement)
    has_been_read = models.BooleanField(default=False)
    email_notifications = False

    def __unicode__(self):
        return unicode(self.content_object)

    def html(self, mode):
        return content_object.html(mode)

    def on_view(self, request):
        if not self.has_been_read:
            return {'new': True}

    class WIDEIO_Meta:
        NO_DRAFT = 1
        permissions = dec.perm_read_logged_users_write_for_admin_only
        sort_enabled = ["created_at"]
        sort_default = {"created_at": 1}
        icon = "icon-flag"
        class Actions:
            @wideio_action(possible=lambda x,r: x.notified_user==r.user)
            def archive(self,request):
                self.wiostate='A'
                self.save()
                
            

# < BUGFIX : For some awkward reason, Notification become null in the current context - that needs to be understood.
Announcement.Notification = Notification
MODELS.append(Notification)
MODELS += get_dependent_models(Notification)


@wideio_owned("author", public="public")
@wideio_timestamped
@wideiomodel
class Comment(models.Model):

    """
    Comments are put on an object... and the object
    have followers ... in a way it is a special kind of announcement
    """
    API = True
    body = models.TextField(
        verbose_name="Comment",
        app_blank=False,
        db_blank=False,
        app_null=False,
        db_null=False,
        max_length=ANNOUNCEMENT_MAX_LENGTH)
    #content_type = models.ForeignKey(
    #    ContentType,
    #    null=True,
    #    verbose_name='content type',
    #    related_name="content_type_set_for_%(class)s",
    #    default=None,db_index=True)
    public=models.BooleanField(default=True)
    object_pk = models.TextField('object ID', null=True,db_index=True)
    content_object = gen2.GenericForeignKey2(
        #ct_field="content_type",
        fk_field="object_pk")

    def __unicode__(self):
        return self.body

    def on_add(self, request):
        if self.author==None:
          self.author=request.user
        if hasattr(self.content_object, "on_comment"):
            getattr(self.content_object, "on_comment")(request, self)
        for f in request.user.get_followers():
            n = Notification()
            n.notified_user = f
            n.content_object = self
            self.owner=request.user

            #send email to followers
            utils.wio_send_email(
                request,
                consent="new_comment",
                subject='you have a new comment'
                (str(self),
                 ),
                template='static/email_comment.html',
                context={
                    'x': self ,
                    'author': self.owner,
                    'comment' : self.content_object
                    },
                to_users=[f],
                url_notification=None)            




            n.save()
        if self.content_object is not None:
            return {"_redirect": self.content_object.get_view_url()}

    def on_update(self, request):
        for f in request.user.get_followers():
            n = Notification()
            n.notified_user = f
            n.content_object = self
            n.save()
        if self.content_object is not None:
            return {"_redirect": self.content_object.get_view_url()}

    @staticmethod
    def can_add(request):
        return request.user.is_authenticated()

    def can_update(self, request):
        return self.wiostate == 'U'

    class WIDEIO_Meta:
        CAN_TRANSFER=False
        #NO_DRAFT=True
        sort_enabled = ["author", "created_at", "object_pk"]
        search_enabled = [ "body", "object_pk", "author" ]        
        form_exclude = [ "public" ]
        permissions = dec.perm_for_logged_users_only

MODELS.append(Comment)
MODELS += get_dependent_models(Comment)


@wideio_owned()
@wideio_timestamped
#@wideio_followable(RelThrough)
@wideiomodel
class CalendarEvent(models.Model):

    """
    TODO: (PRIORITY LOW | Link with the challenge apps)

    For the day when we have a calendar app
    """
    date = models.DateTimeField()
    what = models.TextField()
    organized_by = models.ForeignKey(User)

    def __unicode__(self):
        return unicode(self.when)

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
        icon = "icon-calendar"

MODELS.append(CalendarEvent)
MODELS += get_dependent_models(CalendarEvent)


@wideio_owned(personal=2)
@wideio_timestamped
@wideiomodel
class BlockedUser(models.Model):

    """
    Used to represent the fact that a user doesn't want to hear about another user
    """
    blocked_user = models.ForeignKey(wm.UserAccount, related_name="blocked_by")

    @staticmethod
    def can_add(request):
        return (r.user not in o.blocked_by)

    def on_add(self, r):
        if blocked_user.blocked_by.all().count() > 15:
            from backoffice.lib import log_debug
            log_debug("Span user has been deactivated")
            # < ADDS MORE NOTIFICATIONS TO SYSADM
            blocked_user.is_active = False
            blocked_user.save()

    class WIDEIO_Meta:
        pass


MODELS.append(BlockedUser)
MODELS += get_dependent_models(BlockedUser)

# FIXME: Apparently we don't manage to add the relaction in the module


@wideio_relaction(wm.UserAccount,
                  "block user",
                  icon='ion-bug',
                  possible=lambda s,
                  r: (r.user.is_authenticated() and r.user != s),
                  mimetype="text/html")
def block_user(self, x=None):
    return HttpResponseRedirect(
        DeploymentRequest.get_add_url() +
        "?blocked_by=" +
        self.id)


@wideio_name_invokable({})
@wideio_publishable()
@wideiomodel
class Skill(models.Model):
    ctrname_p = "skill"
    verbose_name_plural = "skills"
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def get_all_references(self, st):
        return []

MODELS.append(Skill)
MODELS += get_dependent_models(Skill)


@wideio_owned("issued_by",personal=0)
@wideio_publishable()
@wideiomodel
class SkillEndorsement(models.Model):
    ctrname_p = "endorsement"
    verbose_name_plural = "endorsements"
    who = models.ForeignKey(wm.UserAccount, db_index=True)
    what = models.ForeignKey(Skill, db_index=True)

    def get_count(self):
        return SkillEndorsement.objects.filter(
            who=self.who,
            what=self.what).count()

    def get_all_references(self, st):
        return [what, who, issued_by]

    def on_add(self, request):
        if SkillEndorsement.objects.filter(
                who=self.who,
                what=self.what,
                issued_by=self.issued_by,
                id__neq=self.id).count() != 0:
            self.delete()
            return {'_redirect': "/"}
        if who == issued_by:
            self.delete()
            #messages.add_message('you can\'t endorse yourself')
            return {'_redirect': "/"}


MODELS.append(SkillEndorsement)
MODELS += get_dependent_models(SkillEndorsement)


@wideio_relaction(
    wm.UserAccount,
    "endorse",
    icon="icon-plus",
    mimetype="text/html",
    xattrs="data-modal-link title=\"Endorse\" ",
    possible=lambda s,
    request: s != request.user)
def endorse_user(user, request):
    from django.http import HttpResponseRedirect
    return HttpResponseRedirect(
        SkillEndorsement.get_add_url() +
        "?who=" +
        user.id)


@wideio_commentable()
@wideio_tagged()
@wideio_publishable()
@wideio_owned("author",personal=0)
@wideio_timestamped
@wideiomodel
class BlogEntry(models.Model):
    ctrname_p = "blog entries"
    verbose_name_plural = "blog entries"
    blog_title = models.CharField(max_length=200)
    text = models.TextField(max_length=10000)

    def can_view(self, r):
        return True

    @staticmethod
    def can_add(r):
        return r.user.is_staff

    def can_update(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False
    
    def can_delete(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False    

    def get_all_references(self):
        from lib.wiotext import wiotext_refs
        refs = [self.author] + wiotext_refs(self.text)
        return refs

    class WIDEIO_Meta:
        icon = "icon-newspaper"

        class Actions:

            @wideio_action(icon="icon-save",
                           possible=lambda o,
                           r: (r.user is not None and r.user.is_staff))
            def publish_and_save(self, request, overwrite=True):
                if self._draft_for_id:
                    return 'window.alert("draft copies may not be saved as such")'
                import yaml
                folder = os.path.join(
                    "initial_fixture",
                    "data",
                    self._meta.db_table)
                if not os.path.exists(folder):
                    os.mkdir(folder)
                filename = os.path.join(folder, self.id) + ".yaml"
                d = self.__dict__
                d = dict(filter(lambda i: i[0] != "_", d.items()))
                open(filename, "w").write(yaml.dump())
                #os.system("git add "+filename)
                for f in self.get_all_references():
                    if hasattr():
                        f.WIDEIO_Meta.publish_and_save(f, request, False)
                return 'window.alert("done")'

MODELS.append(BlogEntry)



@wideio_commentable()
@wideio_tagged()
@wideio_owned("author",personal=0)
@wideio_timestamped
@wideiomodel
class QNA(models.Model):
    ctrname_p = "questions and answers"
    verbose_name_plural = "questions and answers"    
    question=models.TextField(max_length=500)
    additional_details=models.TextField(max_length=3000)    
    votes=models.IntegerField(default=0)
    answers=ef.RerankableParagraphField()
    class WIDEIO_Meta:
        icon = "ion-help"
        form_exclude=['votes','answers']
        mandatory_fields=['question']
        permissions = dec.perm_for_logged_users_only
        
MODELS.append(QNA)        


@wideio_timestamped
@wideiomodel
@wideio_setnames("app integration")
class Integration(models.Model):
    verbose_name_plural = "app integrations"
    is_public=models.BooleanField(default=True)
    
    module=models.CharField(max_length=32,
        choices=map(lambda x:(x['model'], x['name']), settings.INTEGRATION_DOCUMENT_TYPES))
    config=JSONField(null=True, blank=True)
    
    supports_documents=models.BooleanField(default=False)
    supports_notifications=models.BooleanField(default=False)
    team=models.ForeignKey('accounts.Team',blank=True,null=True,db_index=True)

    def __unicode__(self):
        inst = self.get_instance()
        return unicode(inst)

    def get_icon(self):
        return self.get_instance().icon if self.get_instance() else ""

    def get_instance(self):
        if not self.module:
            return None
        if not hasattr(self, 'instance'):
            fromlist = ["network","integrations"]
            module = __import__(
                              ".".join(fromlist + [self.module.lower(), 'integration']),
                              fromlist=fromlist
                             )
            try:
                setattr(self, 'instance', module.Integration(
                    self,
                    self.team,
                    self.config
                ))
            except:
                setattr(self, 'instance', None)
        return self.instance

    def can_view(self, r):
        return True

    def on_view(self, request):
        return {"instance": self.get_instance()}

    def validate(self, request):
        if request.user not in self.team.admins.all():
            raise PermissionException
        integ = self.get_instance()
        if self.supports_notifications:
            if hasattr(integ, "register_webhooks"):
                integ.check_webhooks()
        if self.supports_documents:
            if hasattr(integ, "sync"):
                try:
                    integ.sync() # ensure that documents exists 
                except:
                    messages.add_message(request, messages.WARNING, "Something failed while attempting to sync, please check the configuration")
                    self.save()
                    return False

    def on_add(self, request):
        integ = self.get_instance()
        self.supports_documents = integ.PROVIDES_DOCUMENTS
        self.supports_notifications = integ.PROVIDES_NOTIFICATIONS
        self.save()
        if not self.validate(request):
            return {'_redirect': self.get_update_url()}

    def on_update(self, request):
        if not self.validate(request):
            return {'_redirect': self.get_update_url()}
    
    def can_update(self, request):
       return request.user in self.team.admins.all()
       
    def can_delete(self, request):
       return request.user in self.team.admins.all()

    class WIDEIO_Meta:
        icon = "icon-newspaper"
        mandatory_fields = ['module','team', 'config']
        permissions = dec.perm_for_logged_users_only
        search_enabled = ['module', 'team']
        form_exclude = ['module', 'supports_documents', 'supports_notifications', 'team']
        class Actions:
            @wideio_action(possible=lambda o,r:
                ((o.supports_documents) and
                (r.user in o.team.members.all()))
                )
            def create_new_document(self,request):
                pass

            @wideio_action(possible=lambda o,r:(hasattr(o.get_instance(), 'add_enabled') and(r.user in o.team.members.all())))
            def add_document(self,request):
                return {
                    '_redirect': self.get_base_url() + '/subview/%s/%s/' % ('add_shared_document', self.id)
                }

            @wideio_action(possible=lambda x,r:(r.user in x.team.admins.all()))
            def sync(self, request):
                self.get_instance().sync()
                return self
        class Views:
            @staticmethod
            def config_form(request,id):
                self = Integration.objects.get(id=id)
                form =self.get_instance().get_new_document_form(request)
                if request.method=="POST":                    
                   form =self.get_instance().validate_new_document_form(request)

            @staticmethod
            def add_shared_document(request, id):
                self = Integration.objects.get(id=id)
                instance = self.get_instance()
                ctx = instance.get_new_document_form(instance, request)
                return ctx

            @staticmethod
            def webhook(request,id):
                self=Integration.objects.get(id=id)
                self.get_instance().handle_webhook(request)

MODELS.append(Integration)



@wideio_commentable()
@wideio_tagged()
@wideio_owned("author",personal=0)
@wideio_timestamped
@wideiomodel
@wideio_setnames('document')
class SharedDocument(models.Model):
    verbose_name_plural = "documents"
    is_public=models.BooleanField(default=False)
    provider=models.ForeignKey(Integration)
    descriptor=JSONField(null=True)
    team=models.ForeignKey('accounts.Team',blank=True,null=True,db_index=True)

    def __unicode__(self):
        return unicode(self.get_document())

    def get_document(self):
        return self.provider.get_instance().decode_document(self.descriptor)

    def can_view(self, r):
        return True

    @staticmethod
    def can_add(request):
      """
      Document can't be added directly - the integration provider must be used.
      """
      return hasattr(request.GET, 'provider') and Integration.objects.filter(provider=request.GET['provider']).count()
    
    def on_view(self, request):
       return self.provider.get_instance().on_view(self.descriptor)

    def can_update(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False
    
    def can_delete(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False    

    def get_all_references(self):
        from lib.wiotext import wiotext_refs
        refs = [self.author] 
        return refs

    class WIDEIO_Meta:
        icon = "icon-paperclip"
        mandatory_fields=['title','tags']
        permissions = dec.perm_for_logged_users_only
        search_enabled = ['provider']
MODELS.append(SharedDocument)

@wideio_owned()
@wideio_timestamped
@wideiomodel
class PostponedMailPart(models.Model):
    """ Message to be sent to the user at a specified frequency if the user has made consent for a lower frequency email """
    frequency=models.CharField(max_length=1, default='D', choices=[('H','Hourly'),('D','Daily'),('W','Weekly')])
    text=models.TextField(max_length=10000)
    class WIDEIO_Meta:
        NO_DRAFT=1
        DISABLE_VIEW_ACCOUNTING=1
        icon = "icon-newspaper"    
        permissions = dec.perm_for_admin_only
        
MODELS.append(PostponedMailPart)    

@wideio_owned()
@wideio_timestamped
@wideiomodel
class GiveAction(models.Model):
    """The action of giving an object from one person to another"""
    givestate=models.CharField(max_length=1, default='O', choices=[('O','Offered'),('A','Accepted'),('R','Rejected'),('C','Cancelled')])
    text=models.TextField(max_length=1000)
    from_user=models.ForeignKey(wm.UserAccount,related_name="gave") #< FIXME:MAY BE A TEAM
    to_user=models.ForeignKey(wm.UserAccount,related_name="received") #< FIXME:MAY BE A TEAM
#    content_type = models.ForeignKey(
#        ContentType,
#        null=True,
#        verbose_name='content type',
#        related_name="content_type_set_for_%(class)s")
    object_pk = models.TextField('object ID', null=True)
    content_object = gen2.GenericForeignKey2(
#        ct_field="content_type",
        fk_field="object_pk")

    def on_add(self,request):
         allowed=False
         allowed= allowed or request.user.is_superuser
         allowed= allowed or request.user == getattr(self.content_object,self.content_object.WIDEIO_Meta.owner_field)
         if not allowed:
             try:
               self.delete()
             except:
               pass
             raise PermissionError
         else:
                n=Notification()
                n.notified_user = self.to_user
                n.content_object = Announcement.create_internal(
                None,
                str(self.from_user) + " is inviting you to take the ownership of  "+str(self.content_object),
                self,
                comments_opened=False)
                n.save()                                          
        
    class WIDEIO_Meta:
        NO_DRAFT=1
        DISABLE_VIEW_ACCOUNTING=1
        icon = "icon-newspaper"  
        form_exclude=["givestate"]
        
        permissions = dec.perm_for_admin_only
        class Actions:
            @wideio_action(possible=lambda x,r:(x.givestate=='O') and ((r.user.is_superuser) or (r.user == x.to_user)))
            def accept(self, request):            
                setattr(self.content_object,self.content_object.WIDEIO_Meta.owner_field,request.user)
                self.givestate='A'
                self.save()
                
                n=Notification()
                n.notified_user = self.from_user,
                
                n.content_object = Announcement.create_internal(
                None,
                "Your party has accepted the offering of "+x.content_object,
                self,
                comments_opened=False)
                n.save()                                
                
                return 'alert("done!");'                
                
                
            @wideio_action(possible=lambda x,r:(x.givestate=='O') and ((r.user.is_superuser) or (r.user == x.to_user)))
            def reject(self, request):                            
                self.givestate='R'
                self.save()
                
                n=Notification()
                n.notified_user = self.from_user,
                
                n.content_object = Announcement.create_internal(
                None,
                "Your party has rejected the offering of "+x.content_object,
                self,
                comments_opened=False)
                n.save()                
                
                return 'alert("done!");'                
                
            @wideio_action(possible=lambda x,r:(x.givestate=='O') and ((r.user.is_superuser) or (r.user == x.from_user)))
            def cancel(self, request):
                self.givestate='C'
                self.save()
                
                n=Notification()
                n.notified_user = r.user
                
                n.content_object = Announcement.create_internal(
                None,
                "You have cancelled your offering of "+x.content_object,
                self,
                comments_opened=False)
                n.save()                
                return 'alert("done!");'
        
MODELS.append(GiveAction)
