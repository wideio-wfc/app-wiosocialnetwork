# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

from wioframework.amodels import models, wideiomodel, wideio_setnames
from django.utils.safestring import mark_safe

class WeblinkDocument(models.Model):
    url = models.URLField()
    def __unicode__(self):
        return 'Weblink to <a href="{url}">{url}</a>'.format(**{'url': self.url})

from django import forms
from wiocore.captcha import fields as cfields

class WeblinkForm(forms.Form):
    url = forms.URLField()
    class Meta:
        model = WeblinkDocument

@wideio_setnames('link')
class Integration:
    """
    An Intergration of weblinks, forwards event
    """
    PROVIDES_UPDATE=False
    PROVIDES_DOCUMENTS=True
    PROVIDES_NOTIFICATIONS=False
    icon = '/media/images/integrations/weblinks.png'

    add_enabled = True

    def __init__(self, integration, team, config):
        self.integration=integration
        self.team=team
        self.config=config

    def __unicode__(self):
        return self.ctrname_s

    def sync(self):
        pass
    
    @classmethod
    def do_add(cls,request):
        pass
        
    def on_view(self, doc, request):
        return {'document': self.decode_document(doc) or "example"}

    @staticmethod
    def get_new_document_form(self, request):
        ctx = {}
        form = WeblinkForm(request.REQUEST)
        form.full_clean()
        ctx['_template'] = 'generic/gen_add.html'
        ctx['form'] = form
        ctx.update({
            'appvalidated': form.is_valid
        })
        if request.method=="POST":                    
            if form.is_valid():
                from network.models import SharedDocument
                nd = SharedDocument()
                nd.descriptor = form.cleaned_data
                nd.provider = self.integration
                nd.team = self.integration.team
                nd.save()
                return {'_redirect': self.integration.get_view_url()}
        return ctx

    def decode_document(self, json):
        doc = WeblinkDocument()
        for (key, value) in json.items():
            setattr(doc, key, value)
        return doc