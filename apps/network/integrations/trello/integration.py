# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from django import forms
import trolly

class Integration:
  """
  "An Intergration here forwards event"
  In the case of overleaf it makes a new type of document available
  these are different types of integration
  """
  PROVIDES_UPDATE=True
  PROVIDES_DOCUMENTS=True
  PROVIDES_NOTIFICATIONS=True
  API_KEY = "5b50047b39f971264343f76734fc8475"
  icon = '/media/images/integrations/trello.png'

  class ConfigForm(forms.Field):
    token=forms.CharField(max_length=128)
    organisation_name_or_id=forms.CharField(max_length=128)

  # dbe2fa6cb4389c2c35cb13439aefb5242844c7751dcccc224a65bbbe7273df93
  def __init__(self, integration, team, config):
    self.integration=integration
    self.team=team
    self.config=config
    
    import trolly.client
    if type(self.config) in [dict] and "auth_token" in self.config:
      self.client = trolly.client.Client(self.API_KEY, self.config["auth_token"])
    else:
      self.client = None

  def __unicode__(self):
      return "Trello"

  def config_view(self,request):
      f = ConfigForm(request.POST)
      return {
               'form': f,
               "authorize_url": "https://trello.com/1/authorize?key="+self.API_KEY+"&name=wide.io&expiration=never&response_type=token"
             }

  # token -> a307f2cf3d979dbed2d5c8226607a44abb57a0ee9ca24229fac0b07c12325f18
  def sync(self):
      from network.models import SharedDocument
      for b in trolly.organisation.Organisation(self.client, "me").get_boards():
          ri = b.getBoardInformation()
          if not ri["closed"]:
              sd = SharedDocument()
              sd.is_public = ri["permisisonLevel"]=="public"
              sd.provider=models.ForeignKey(Integration)
              sd.descriptor={'type':'board','id':ri["id"]}
              sd.team=team
      for b in trolly.organisation.Organisation(c,"wideio").get_boards():
          pass
  
  def setup_webhooks(self):
    pass
   
  def document_on_view(self,doc):
    ri = be.getBoardInformation()
    return {"_redirect": ri["url"]}
  
  @classmethod
  def webhook(cls,request):
    ## PASS: Ok we forward the trello events/notifications according to the follow rules
    pass
  
  @classmethod
  def on_add(cls,request):
    ## PASS: Ok we forward the trello events/notifications according to the follow rules
    pass
  
  @classmethod
  def on_delete(cls,request):
    ## PASS: Ok we forward the trello events/notifications according to the follow rules
    pass
  
  @classmethod
  def on_update(cls,request):
    cls.on_delete(request)
    cls.on_add(request)