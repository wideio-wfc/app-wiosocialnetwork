# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import wioframework.fields as models
from wioframework.amodels import *

from wioframework import decorators as dec

@wideio_commentable()
@wideio_tagged()
@wideio_owned("author",personal=0)
@wideio_timestamped
@wideiomodel
@wideio_setnames('article')
class OverLeafArticle(models.Model):
    verbose_name_plural = "articles"
    title = models.CharField(max_length=200,blank=False)
    url = models.URLField(max_length=256,blank=True)
    team=models.ForeignKey('accounts.Team', blank=True, null=True, db_index=True)

    def can_view(self, r):
        return True

    def on_add(self,request):
      import mechanize
      import urllib
      data=urllib.urlencode({"snip_uri":"https://dl.dropbox.com/u/31383671/site/helloworld.tex",
            "splash":"False"
           })
      br=mechanize.Browser()
      r=br.open("https://www.overleaf.com/docs",data)
      self.url=r.geturl()
      self.save()
    
    def on_view(self,request):
       return {"_redirect":self.url}

    def can_update(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False
    
    def can_delete(self, r):
        if r.user.is_staff:
            return True
        try:
            if (r.user.id == self.author_id):
                return True
        except:
            pass
        return False    

    def get_all_references(self):
        from lib.wiotext import wiotext_refs
        refs = [self.author] 
        return refs

    class WIDEIO_Meta:
        icon = "icon-newspaper"
        mandatory_fields=['title','tags']
        permissions = dec.perm_for_logged_users_only

class Integration:
  PROVIDES_UPDATE=False
  PROVIDES_DOCUMENTS=True
  PROVIDES_NOTIFICATIONS=False
  icon = '/media/images/integrations/overleafarticle.png'

  def __init__(self, integration, team, config):
    self.intergration=integration
    self.team=team
    self.config=config

  def __unicode__(self):
    return "OverLeafArticle"

  def sync(self):
      pass
  
  @classmethod
  def do_add(cls,request):
      pass
      
  def on_view(self,doc,request):
    cls.on_delete(request)
    cls.on_add(request)
    
  def on_view(self,doc,request):
    cls.on_delete(request)
    cls.on_add(request)