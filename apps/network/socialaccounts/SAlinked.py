# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import datetime
import sys
import os
import re
from social_auth.backends.contrib import linkedin
import urllib
import urlparse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotAllowed

class LinkedInAccount:

    """
    This class is here to ensure that we can link with a linkedin LinkedInAccount
    Display information about the user
    and optionally, download and link some data if necessary


    login_details are storred in the encrypted field of account and a special form shall be provided.
    """
    URL_IS_USED = True
    # http://dblp.uni-trier.de/pers/hd/n/Nouvel:Bertrand
    # URL_REGEXP = "http://dblp.uni-trier.de/pers/([A-Za-z0-9]+)/([A-Za-z0-9] )/([A-Za-z0-9]+)"
    URL_REGEXP = "https?://(www\.)?linkedin.com/.*"

    class Form:
        pass

    @staticmethod
    def on_first_connect(user, socialaccount):
        ##
        ## import bibliography, profressional experiences, schools, academic degrees
        ##
        pass

    @staticmethod
    def on_sync(user, socialaccount):
        account_type = "linkedin"
        ## import bibliography
        response = socialaccount
        print socialaccount

        business_profile = user.get_business_profile()
        scientific_profile = user.get_scientific_profile()

        if not user.socialaccount_rev_owner.filter(account_type=account_type).count():
            from accounts.models import SocialAccount
            nua = SocialAccount()
            nua.account_type = account_type
            if 'id' in response:
                nua.account_url = 'www.linkedin.com/profile/view?id=' + response['id']
            nua.save()
            user.socialaccount_rev_owner.add(nua)

        if 'positions' in response:
            positions = response['positions'].get('position', [])
            if isinstance(positions, dict):
                positions = [positions]
            for position in positions:
                is_current = position.get('is-current', 'false') == 'true'
                company = position.get('company', {})
                sector = company.get('industry', None)
                if business_profile is not None:
                    if is_current:
                        # the size return a string that contains an estimation,
                        # e.g. '51-200 employees'
                        nb_emp = re.findall(
                            '\\d+,?\\d?\\d?\\d?',
                            company.get(
                                'size',
                                ''))
                        minimum = 500000
                        for nb in nb_emp:
                            nb = nb.replace(',', '')
                            if int(nb) < minimum:
                                minimum = int(nb)
                        if minimum != 500000 and not business_profile.number_of_employees:
                            business_profile.number_of_employees = minimum
                        for s in business_profile.INDUSTRY_CHOICES:
                            if s[1] == sector and not business_profile.sector:
                                business_profile.sector = s[0]
                                break
                elif scientific_profile is not None:
                    # fill in scientific profile from positions
                    pass

        if business_profile is not None:
            business_profile.save()

        if scientific_profile is not None:
            if not scientific_profile.research:
                scientific_profile.research = response['summary']
            # if not scientific_profile.research_interests:
            #     scientific_profile.research_interests = response['interests']
            scientific_profile.save()

        if hasattr(user, "school_rev_owner"):
            educations = response['educations'].get('education')
            if isinstance(educations, dict):
                educations = [educations]
            for education in educations:
                from accounts.models import School, Organisation
                ns = School()
                ns.cursus = education.get('degree', '')
                start = education.get('start-date', {'year':'2015'}).get('year')
                end = education.get('end-date', {'year':'2015'}).get('year')
                start_month = education.get('start-date').get('month') or '01'
                end_month = education.get('end-date').get('month') or '01'
                ns.from_date = start + '-' + start_month + '-01'
                ns.to_date = end + '-' + end_month + '-01'
                school = education.get('school-name', '')
                if user.school_rev_owner.filter(organisation__name=school).count():
                    pass
                else:
                    if school:
                        org = Organisation.invoke(school)
                        ns.organisation = org
                    ns.save()
                    user.school_rev_owner.add(ns)
            user.save()
        pass

    @staticmethod
    def on_disconnect(user, socialaccount):
        ##
        ## import bibliography
        ##
        pass


INPUT_METADATA = {
    "icon": ("css", "fa-envelope"),
    "version": "0.0",
    "oauth": True,
}


def fetch_contacts(handler, authentication, *args, **kwargs):

    api = API_requires_oauth(authentication)
    res = api.get_connections()

    for j in res:
        # print repr(values[j])[:60]
        try:
            c = {}
            c["firstname"] = j[firstName]
            c["lastname"] = j[lastName]
            c["fullname"] = "%s %s" % (j[firstName], j[lastName])
            c["addresses"] = [(1, '%id%')]
            c["country"] = j["country"]
            c["industry"] = j["industry"]
            handler(c)
        except Exception as e:
            print e


def fetch_messages(api):
    from linkedin.linkedin import NETWORK_UPDATES
    # init api
    # get messages
    update_types = (NETWORK_UPDATES.CONNECTION, NETWORK_UPDATES.PICTURE)
    return api.get_network_updates(update_types)


def API_requires_oauth(authentication):
    application = linkedin.LinkedInApplication(authentication)
    return application


def configure_oauth_initial():
    print "BASE URL FOR LINKED_IN IS", settings.BASE_URL + 'com/test_linkedin_followup'
    authentication = linkedin.LinkedInAuthentication(
        settings.LINKED_IN["API_key"],
        settings.LINKED_IN["secret_key"],
        settings.BASE_URL +
        'com/test_linkedin_followup',
        linkedin.PERMISSIONS.enums.values())
    print "AUTH URL", authentication.authorization_url
    print str(linkedin.PERMISSIONS.enums.values())
    return authentication.authorization_url  # authentication,


def configure_oauth_authorise(authorisation_code):  # authentication,
    '''
    GENERATE AUTHENTICATION OBJECT WITH CODE
    '''
    print "BASE URL FOR LINKED_IN IS", settings.BASE_URL + 'com/test_linkedin_followup'
    authentication = linkedin.LinkedInAuthentication(
        settings.LINKED_IN["API_key"],
        settings.LINKED_IN["secret_key"],
        settings.BASE_URL +
        'com/test_linkedin_followup',
        linkedin.PERMISSIONS.enums.values())
    print "AUTH URL", authentication.authorization_url
    authentication.authorization_code = authorisation_code
    authentication.token = authentication.get_access_token()
    return authentication

#@wideio_view_xtra(
#    path="oauth/" +
#    os.path.basename(__file__)[
#        :-
#        4] +
#    "/start/",
#    decorators=[grc_login_required])
#def oauth_start(request):
#    urlto = configure_oauth_initial()
#    return HttpResponseRedirect(urlto)

#@wideio_view_xtra(decorators=[grc_login_required])
#def test_linkedin_followup(request):
#    auth_code = request.GET.get('code')  # request.REQUEST['code']
#    authentication = configure_oauth_authorise(auth_code)