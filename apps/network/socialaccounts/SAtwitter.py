# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import datetime
import sys
import os
import aiosciweb1.settings as settings
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotAllowed

INPUT_METADATA = {
    "icon": ("css", "icon-twitter"),
    "version": "0.0",
    "oauth": True,
}

import tweepy

def configure_oauth_initial():
    import oauth2 as oauth
    from urlparse import parse_qsl
    '''
    GET USER OAUTH
    '''
    auth = tweepy.OAuthHandler(
        settings.TWITTER["API_key"],
        settings.TWITTER["secret_key"])

    REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
    ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'
    SIGNIN_URL = 'https://api.twitter.com/oauth/authenticate'
    try:
        AUTHORIZATION_URL = auth.get_authorization_url()
    except tweepy.TweepError:
        print 'Error! Failed to get request token.'
    return (
        AUTHORIZATION_URL,
        (auth.request_token.key,
         auth.request_token.secret))


# authentication,
def API_requires_oauth(authorisation_token, authorisation_verifier):
    '''
    GENERATE AUTHENTICATION OBJECT WITH CODE
    '''
    print authorisation_token, authorisation_verifier
    auth = tweepy.OAuthHandler(
        settings.TWITTER["API_key"],
        settings.TWITTER["secret_key"])
    auth.set_request_token(
        str(authorisation_token[0]), str(authorisation_token[1]))

    try:
        auth.get_access_token(authorisation_verifier)
    except tweepy.TweepError:
        print 'Error! Failed to get access token.'
    t = tweepy.API(auth)
    return t


@wideio_xtra_view(path="oauth/" + os.path.basename(__file__)[:-4] + "/start/", decorators=[grc_login_required])
def oauth_start(request):
    oauth_init_response = sntt.configure_oauth_initial()
    urlto = str(oauth_init_response[0])
    request.session['request_token'] = oauth_init_response[1]
    return HttpResponseRedirect(urlto)


@wideio_xtra_view(
    path="oauth/" +
    os.path.basename(__file__)[
        :-
        4] +
    "/followup/",
    decorators=[grc_login_required])
def oauth_followup(request):
    verifier = str(request.GET.get('oauth_verifier'))
    token = request.session['request_token']
    del request.session['request_token']
    api = API_requires_oauth(token, verifier)
    return HttpResponse(str(api.me().screen_name))


class TwitterAccount:

    """
    This class is here to ensure that we can link with a twitter TwitterAccount
    Display information about the user
    and optionally, download and link some data if necessary


    login_details are storred in the encrypted field of account and a special form shall be provided.
    """
    URL_IS_USED = True
    # http://dblp.uni-trier.de/pers/hd/n/Nouvel:Bertrand
    URL_REGEXP = "http://dblp.uni-trier.de/pers/([A-Za-z0-9]+)/([A-Za-z0-9] )/([A-Za-z0-9]+)"

    class Form:
        pass

    @staticmethod
    def on_fist_connect(user, socialaccount):
        ##
        ## import bibliography
        ##
        pass

    @staticmethod
    def on_sync(user, socialaccount):
        ##
        ## import bibliography
        ##
        pass

    @staticmethod
    def on_disconnect(user, socialaccount):
        ##
        ## import bibliography
        ##
        pass