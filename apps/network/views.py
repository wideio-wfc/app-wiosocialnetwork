# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# Create your views here.
from django.core.paginator import Paginator, InvalidPage, EmptyPage
# DJANGO uses the shortcut framework which I consider slow and buggy
#from django.contrib.auth.decorators import login_required, user_passes_test
from wioframework.decorators import login_required, user_passes_test
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from wioframework.jsonresponse import JsonResponse

from django.template import defaultfilters
from datetime import datetime, time
from wioframework import utilviews as uv
from network import models
from django.contrib import messages
from wioframework import decorators as dec
from functools import reduce


def notification_count(request):
    if (not request.user.is_authenticated()):
        return JsonResponse({"notification_count": -1})
    nb = models.Notification.objects.filter(
        notified_user=request.user).filter(
        has_been_read=False).count()
    return JsonResponse({"notification_count": nb})
notification_count.default_url = 'notificationcount'


def reset_notification_count(request):
    if (not request.user.is_authenticated()):
        return JsonResponse({"notification_count": -1})
    models.Notification.objects.filter(
        notified_user=request.user).update(
        has_been_read=True)
    #~ return HttpResponseRedirect("/network/notification/list/?notified_user_id=" + request.user.id)
    return HttpResponseRedirect("/")
reset_notification_count.default_url = 'resetnotificationcount'


def notification_list(request):
    if (not request.user.is_authenticated()):
        return JsonResponse({"notifications": []})
    return JsonResponse({"notifications": list(
        models.Notification.objects.filter(notified_user=request.user))})
notification_list.default_url = 'notificationlist'

VIEWS = reduce(lambda x, y: x + uv.AUDLV(y),models.MODELS,[]) + [notification_count,
                      reset_notification_count,
                      notification_list]
